# Avanzando con el modelo

Después de pensarlo un poco el modelo al que queremos llegar tiene
esta pinta^[Obviamente esta solución no es única, y seguro que no es
la mejor, lo bueno es que si veis fallos podéis mejorarla a vuestro
gusto]:

Así pues

#. Cada cosa tiene un proveedor(_provider_)
#. Cada cosa está en un sitio, nos inventamos un sitio genérico (_container_)
#. Un _container_ puede estar dentro de otro _container_ o puede estar
   en una sala (_room_), pero no las dos cosas a la vez.
#. Cada sala está en un lugar (_place_)

Queremos tener controlados los proveedores, por si necesitamos reponer
existencias.

Los contenedores genéricos son sencillos y flexibles, pueden
representar cualquier cosa: una caja (seguramente tendremos que añadir
códigos a las cajas de la asociación), un armario, el area de
soldadura, todo son contenedores. Como son anidables podemos tener una
cajonera (que será un contenedor) y cada cajón de la misma será otro
contenedor. O una estánteria (contenedor) que contiene baldas que, a
su vez, son contenedores.

Los contenedores están en salas (_room_). En BricoLabs tenemos varias
salas.

Y por último las salas están en alguna localización (_place_)


## Temas pendientes

Seguramente tendremos que refinar este modelo o extenderlo, algunos
temas que dejamos para el futuro:

* Gestionar proyectos.
* Gestionar prestamos.
* Gestionar categorias para clasificar las cosas. Ejemplos de
  categorias serían: "Herramienta", "Componente Electrónico",
  "Consumibles", "Maquinas", "Mobiliario"
* Gestionar material almacenado, puede que haya una pequeña cantidad
  de material en un cajón, tornillos por ejemplo, y tener el mismo
  material en un almacén.


~~~~{python}
db.define_table('place',
    Field('id', 'integer'),
    Field('name', 'string',
          length=32,
          unique=True,
          required=True,
          requires=[IS_NOT_EMPTY(),
                    IS_NOT_IN_DB(db, 'place.name')]),
    Field('address', 'string',
    Field('description', 'string'),
    Field('picture', 'upload',
          requires=IS_IMAGE()),
    Field('created_by', 'reference auth_user'),
    Field('created_on', 'datetime'),
    format='%(name)s',
    migrate = True);

db.define_table('room',
    Field('id', 'integer'),
    Field('name', 'string',
          length=32,
          unique= True,
          required=True,
          requires = [IS_NOT_EMPTY,
                      IS_NOT_IN_DB(db, 'room.name')]),
    Field('description', 'string',
          length=512),
    Field('picture', 'upload',
          requires=IS_IMAGE()),
    Field('created_by', 'reference auth_user'),
    Field('created_on', 'datetime'),
    Field('place_id', 'reference place',
          requires=IS_IN_DB(db, 'place.id', '%(name)s')),
    migrate = True);

db.define_table('container',
    Field('id', 'integer'),
    Field('name', 'string',
          length=32,
          unique=True,
          required=True,
          requires = [IS_NOT_EMPTY,
                      IS_NOT_IN_DB(db, 'container.name')]),
    Field('description', 'string',
          length=512),
    Field('picture', 'upload',
          requires=IS_IMAGE()),
    Field('created_by', 'reference auth_user'),
    Field('created_on', 'datetime'),
    migrate = True);

db.define_table('thing',
    Field('id', 'integer'),
    Field('name', 'string',
          length=32,
          unique=True,
          required=True,
          requires = [IS_NOT_EMPTY,
                      IS_NOT_IN_DB(db, 'thing.name')]),
    Field('description', 'string',
          length=512),
    Field('picture', 'upload',
          requires=IS_IMAGE()),
    Field('qty', 'integer'),
    Field('created_by', 'reference auth_user'),
    Field('created_on', 'datetime'),
    Field('container_id', 'reference container',
          requires=IS_IN_DB(db, 'container.id', '%(name)s')),
    migrate = True);

db.define_table('provider',
    Field('id', 'integer'),
    Field('name', 'string',
          length=32,
          unique=True,
          required=True,
          requires = [IS_NOT_EMPTY,
                      IS_NOT_IN_DB(db, 'provider.name')]),
    Field('description', 'string',
          length=512),
    Field('tin', 'string',
          length=16,
          unique=True,
          required=True,
          requires = [IS_NOT_EMPTY,
                      IS_NOT_IN_DB(db, 'provider.tin')],
          label=T('TIN')),
    Field('email', 'string',
          length=32,
          requires = IS_EMPTY_OR(IS_EMAIL())),
    Field('phone', 'string',
          length=32),
    Field('created_by', 'reference auth_user'),
    Field('created_on', 'datetime'),
    migrate = True);

db.define_table('provided_by',
    Field('id', 'integer'),
    Field('part_number', 'string',
          length=32),
    Field('description', 'string',
          length=512),
    Field('thing_id', 'reference thing',
          requires=[IS_NOT_EMPTY,
                    IS_IN_DB(db, 'thing.id', '%(name)s')]),
    Field('provider_id', 'reference provider',
          requires=[IS_NOT_EMPTY,
                    IS_IN_DB(db, 'provider.id', '%(name)s')]),
    migrate = True);

db.define_table('container_location',
    Field('id', 'integer'),
    Field('nested', 'boolean',
          required=True),
    Field('contained', 'reference container',
          unique=True,
          requires = [IS_NOT_EMPTY,
                      IS_IN_DB(db, 'container.id', '%(name)s'),
                      IS_NOT_IN_DB(db, 'container_location.contained')]),
    Field('container_id', 'reference container',
          requires = IS_EMPTY_OR(IS_IN_DB(db, 'container.id', '%(name)s'))),
    Field('room_id', 'reference room',
          requires = IS_EMPTY_OR(IS_IN_DB(db, 'room.id', '%(name)s'))),
    migrate = True);
~~~~
